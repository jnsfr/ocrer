/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr;

import com.beust.jcommander.JCommander;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import ocr.detector.LetterWizard;
import ocr.learn.TrainOCRer;
import ocr.model.Result;
import ocr.params.OCRerParams;
import ocr.params.OCRerTesterParams;
import ocr.params.OCRerTrainerParams;

/**
 *
 * @author jnunomac
 */
public class OCRMain {
    
    public static final Logger LOGGER = Logger.getLogger("log_" + OCRMain.class.getSimpleName() + ".txt");

    static {
        try {
            int limit = 100*1024*1024; // 100 Mb
            int numLogFiles = 100;
            FileHandler fh = new FileHandler("log_" + OCRMain.class.getSimpleName() + ".txt", limit, numLogFiles, true);
            fh.setFormatter(new Formatter() {
                @Override
                public String format(LogRecord record) {
                    StringBuilder buf = new StringBuilder(1000);
                    /*
                     record.getThreadID()+"::"+record.getSourceClassName()+"::"
                     +record.getSourceMethodName()+"::"
                     +new Date(record.getMillis())+"::"
                     +record.getMessage()+"\n";
                     */
                    buf.append("[");
                    buf.append(new java.util.Date(record.getMillis()));
                    buf.append("]");
                    buf.append("[");
                    buf.append(record.getLevel());
                    buf.append("]");
                    buf.append("[");
                    buf.append(record.getThreadID());
                    buf.append("]");
                    buf.append("[");
                    buf.append(record.getSourceClassName());
                    buf.append("]");
                    buf.append("[");
                    buf.append(record.getSourceMethodName());
                    buf.append("]");
                    buf.append(": ");
                    buf.append(formatMessage(record));
                    buf.append('\n');
                    return buf.toString();
                }

            });

            LOGGER.addHandler(fh);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        // mudar para Level.INFO no final .. INFO WARNING e SEVERE sao mostrados
        OCRerParams main = new OCRerParams();
        JCommander jcom = new JCommander(main);
        // available params
        OCRerTrainerParams train;
        OCRerTesterParams test;
        // start command definition
        jcom.addCommand("-train", train = new OCRerTrainerParams());
        jcom.addCommand("-test", test = new OCRerTesterParams());
        // end command definition
        // parse args
        jcom.parse(args);

        LOGGER.log(Level.INFO, "---------------");
        LOGGER.log(Level.INFO, main.toString());

        if (jcom.getParsedCommand() == null) {
            jcom.usage();
            System.exit(0);
            //
        }
        LOGGER.log(Level.INFO, jcom.getParsedCommand());
//        System.exit(0);// tests

        switch (main.verbose) {
            case 0:
                LOGGER.setLevel(Level.OFF);
                break;
            case 1:
                LOGGER.setLevel(Level.INFO);
                break;
            case 2:
                LOGGER.setLevel(Level.ALL);
                break;
        }

//        System.exit(0);// tests
        switch (jcom.getParsedCommand()) {
            case "-train":
                LOGGER.log(Level.INFO, train.toString());
                LOGGER.log(Level.INFO, "---------------");
                TrainOCRer the_trainer = new TrainOCRer(train);
                the_trainer.batchTrain();
                break;
            case "-test":
                LOGGER.log(Level.INFO, test.toString());
                LOGGER.log(Level.INFO, "---------------");
                LetterWizard lw = new LetterWizard();
                lw.load(test.path_to_model);
                lw.batch(test.to_test);
                lw.shutdownThreads();
                break;

        }
        long end = System.currentTimeMillis();
        LOGGER.log(Level.INFO, "!!!!! time elapsed {0} secs == {1} minutes !!!!!", new Object[]{(end - start) / 1000, (end - start) / 60000});
        LOGGER.log(Level.INFO, "!!!!! All done terminating !!!!!");
    }

}
