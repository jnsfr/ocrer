/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.params;

import com.beust.jcommander.Parameter;
import ocr.util.ClassUtils;

/**
 *
 * @author jnunomac
 */
public class OCRerParams {

    @Parameter(names = {"-h", "-help"}, help = true)
    private boolean help;

    @Parameter(names = {"-log", "-verbose"}, description = "Level of verbosity (0 - disable, 1 - Info, 2 - Trace")
    public Integer verbose = 1;

    @Override
    public String toString() {
        return ClassUtils.getClassToString(this, this.getClass()); //To change body of generated methods, choose Tools | Templates.
    }
}
