/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.params;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import ocr.learn.Classifier;
import ocr.util.ClassUtils;

/**
 *
 * @author jnunomac
 */
@Parameters(commandDescription = "The OCRer trainer.")
public class OCRerTrainerParams {

    @Parameter(required = true, names = {"-data"}, description = "Path to the training dataset. "
            + "It should be a directory with sub-directories per class. "
            + "Special folders named \"Positives\"(not implemented yet) \"Negatives\" are used to force a positive and negative training.")
    public String path_to_data;

    @Parameter(required = true, names = {"-ocrconfig", "-ocfg"})
    public String path_to_ocr_config;

    @Parameter(required = true, names = {"-config", "-cfg"})
    public String path_to_config_file;

    @Parameter(required = false, names = {"-nt", "-numthreads"}, description = "Size of the thread pool to be used by the system.")
    public Integer numthreads = 8;

//    @Parameter(required = true, names = {"-ct", "-classifierType"}, description = "Classifier Type")
//    public Integer type = Classifier.classifiertype.SNoW.ordinal();

    @Override
    public String toString() {
        return ClassUtils.getClassToString(this, this.getClass()); //To change body of generated methods, choose Tools | Templates.
    }
}
