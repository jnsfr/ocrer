/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.params;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import ocr.util.ClassUtils;

/**
 *
 * @author jnunomac
 */
@Parameters(commandDescription = "The OCRer tester.")
public class OCRerTesterParams {

    @Parameter(names = {"-model", "-m"})
    public String path_to_model;

    @Parameter(names = {"-test", "-t"})
    public String to_test;
    
    @Override
    public String toString() {
        return ClassUtils.getClassToString(this, this.getClass()); //To change body of generated methods, choose Tools | Templates.
    }
}
