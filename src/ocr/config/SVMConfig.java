/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.config;

import ocr.external.libsvm.Svm_parameter;
import ocr.util.ClassUtils;

/**
 *
 * @author jnunomac
 */
public class SVMConfig extends Svm_parameter {

    public SVMConfig() {
        super();
    }

    public boolean optimize;
    // optimize parameters limits
    public int C0;
    public int C1;
    public int gamma0;
    public int gamma1;
    public int degree0;
    public int degree1;

    @Override
    public String toString() {
        return ClassUtils.getClassToString(this, this.getClass());
    }
}
