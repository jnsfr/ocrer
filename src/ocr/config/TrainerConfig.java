/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.config;

import java.io.Serializable;
import ocr.learn.Classifier;
import ocr.util.ClassUtils;

/**
 *
 * @author jnunomac
 */
public class TrainerConfig implements Serializable {

    public int width;
    public int height;
    public int distinct_values;
    public Classifier.Classifiertype classifier_type;
    public ClassificationStrategy classif_strat;

    public enum ClassificationStrategy {

        OneVsOne, OneVsAll
    };

    @Override
    public String toString() {
        return ClassUtils.getClassToString(this, this.getClass());
    }
}
