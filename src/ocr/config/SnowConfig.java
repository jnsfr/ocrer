/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.config;

import ocr.util.ClassUtils;

/**
 *
 * @author jnunomac
 */
public class SnowConfig {
    
    public int cycles;
    public double alpha;
    public double beta;
    public double threshold;
    public boolean threshold_relative;
    public double initial_value;
    
    @Override
    public String toString() {
        return ClassUtils.getClassToString(this, this.getClass()); 
    }
    
}
