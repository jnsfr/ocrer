/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.detector;

import java.util.concurrent.Callable;
import ocr.learn.Classifier;

/**
 *
 * @author jnunomac
 */
public class LetterWorker implements Callable<double[]> {

    private final Classifier model;
    private final int[] input;

    public LetterWorker(Classifier model, int[] input) {
        this.model = model;
        this.input = input;
    }

    @Override
    public double[] call() throws Exception {
        // classify
        return model.classifySingleInstance(input);
    }

}
