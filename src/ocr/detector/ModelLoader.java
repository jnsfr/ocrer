/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.detector;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import static ocr.OCRMain.LOGGER;
import ocr.learn.Classifier;
import ocr.learn.Classifier.Classifiertype;
import ocr.model.OCRer;
import ocr.model.SVM;
import ocr.model.Snow;

/**
 *
 * @author jnunomac
 */
public class ModelLoader implements Callable<Classifier> {

    private final OCRer ocrer;
    private final Path path_to_ocrer;
    private final String to_model;
    private final Classifiertype the_type;
    private final String label;

    public ModelLoader(Path path_to_ocrer, String to_model, String label, OCRer ref, Classifiertype type) {
        this.ocrer = ref;
        this.path_to_ocrer = path_to_ocrer;
        this.the_type = type;
        this.to_model = to_model;
        this.label = label;
    }

    @Override
    public Classifier call() throws IOException {
        Classifier c = null;
        if (the_type == Classifier.Classifiertype.SNOW) // create and load !
        {
            LOGGER.log(Level.INFO, "Loading as SNOW -> {0} ", Paths.get(path_to_ocrer.toString(), to_model));
            c = new Snow(this.ocrer.getW(), this.ocrer.getH(), this.ocrer.getN_colors());

        } else {
            LOGGER.log(Level.INFO, "Loading as SVM -> {0}", Paths.get(path_to_ocrer.toString(), to_model));
            c = new SVM(this.ocrer.getW(), this.ocrer.getH(), this.ocrer.getN_colors());
        }
        c.setLabel(label);
        c.load(Paths.get(path_to_ocrer.toString(), to_model));
        return c;
    }

}
