package ocr.detector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import static ocr.OCRMain.LOGGER;
import ocr.config.TrainerConfig;
import ocr.config.TrainerConfig.ClassificationStrategy;
import ocr.learn.Classifier;
import static ocr.learn.Classifier.CLASSE;
import static ocr.learn.Classifier.CONFIDENCE;
import static ocr.learn.Classifier.NEGATIVE;
import static ocr.learn.Classifier.POSITIVE;
import ocr.model.ClassifierInfo;
import ocr.model.Image;
import ocr.model.OCRer;
import ocr.model.Result;
import ocr.model.Snow;
import ocr.params.OCRerTesterParams;
import org.apache.commons.lang.StringUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jnunomac
 */
public class LetterWizard {
    
    private HashMap<String, Classifier> classifiers;
    private ArrayList<String> classes;
    private OCRer ocrer;
    private final ExecutorService threadPool;

    /**
     * The LetterWizard default Constructor
     */
    public LetterWizard() {
        this.classifiers = new HashMap<>();
        this.classes = new ArrayList<>();
        this.threadPool = Executors.newFixedThreadPool(10);
    }

    /**
     * Shutdowns threads
     */
    public void shutdownThreads() {
        this.threadPool.shutdown();
        try {
            this.threadPool.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected Object clone() {
        try {
            return super.clone(); //To change body of generated methods, choose Tools | Templates.
        } catch (CloneNotSupportedException ex) {
            LOGGER.log(Level.SEVERE, "could not clone! " + ex);
            return null;
        }
    }

    /**
     * Loads the OCRer model.
     *
     * @param path_to_ocrer
     */
    public void load(File path_to_ocrer) {
        load(Paths.get(path_to_ocrer.getAbsolutePath()));
    }

    /**
     * Loads the OCRer model.
     *
     * @param path_to_ocrer
     */
    public void load(String path_to_ocrer) {
        load(Paths.get(path_to_ocrer));
    }

    /**
     * Loads the OCRer model.
     *
     * @param path_to_ocrer
     */
    public void load(Path path_to_ocrer) {
        
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            // load the main file for the OCR
            this.ocrer = gson.fromJson(new FileReader(new File(path_to_ocrer.toFile(), "ocrer.model")), OCRer.class);
            
            ArrayList<Future<Classifier>> loader_list = new ArrayList<>();
            for (ClassifierInfo cinfo : this.ocrer.getPath_to_models()) {
                loader_list.add(threadPool.submit(new ModelLoader(path_to_ocrer, cinfo.getPath(),
                        cinfo.getLabel(), ocrer, Classifier.Classifiertype.valueOf(cinfo.getType()))));
            }
            // for each file of the OCR create threads
            for (Future<Classifier> created : loader_list) {
                Classifier c;
                try {
                    c = created.get();
                } catch (InterruptedException | ExecutionException ex) {
                    LOGGER.log(Level.SEVERE, "Problems loading one or more classifiers.. OCRer set to null: {0}", ex);
                    this.ocrer = null;
                    return;
                }
                classes.add(c.getLabel());
                classifiers.put(c.getLabel(), c);
            }
            //
        } catch (FileNotFoundException ex) {
            System.out.println("Could not load : " + path_to_ocrer.toString() + " : " + ex.getLocalizedMessage());
            LOGGER.log(Level.SEVERE, "Could not find : {0} : {1}", new Object[]{path_to_ocrer.toString(), ex.getLocalizedMessage()});
        }
        
    }

    /**
     * Evaluates a input image with the classifier at classif_index
     *
     * @param classif_index index of the classifier (if out bounds exception is
     * thrown)
     * @param input pixels of the image in [0 255]
     * @return value of the
     */
    public double evaluateasCharacter(int classif_index, int[] input) {
        
        String key = classes.get(classif_index);
        Classifier c = classifiers.get(key);

        // calcular apenas a resposta de um classificador
        // preprocess
        int[] to_classify = c.preprocessImage(input);
        // classify
        double[] c_result = c.classifySingleInstance(to_classify);
        return c_result[CONFIDENCE];
    }

    /**
     * Receives an array containing input pixels [0-255] and outputs the Result
     * from the OCR process. (do some magic!)
     *
     * @param input pixels in [0-255] in a single integer array
     * @return an object containing information about the OCR process
     */
    public Result test(int[] input) {
        
        Result res = new Result();

        // OCR process
        // 
        int num_classes = ClassificationStrategy.valueOf(this.ocrer.getStrat()) == ClassificationStrategy.OneVsOne
                ? this.ocrer.getN_classes() : classes.size();
        
        double[] positive_activations = new double[num_classes];
        double[] negative_activations = new double[num_classes];
        double[] confidences = new double[num_classes];
        double[] diffs_activations = new double[num_classes];
        double[] c_result;
        double detection_value = -Float.MAX_VALUE;
        int[] to_classify;
        
        if (ClassificationStrategy.valueOf(ocrer.getStrat()) == ClassificationStrategy.OneVsOne) {
            LOGGER.log(Level.INFO, " One vs One classifier !");
            for (Classifier c : classifiers.values()) {
                c_result = c.classifySingleInstance(c.preprocessImage(input));
//                System.out.println(c_result.length);
                System.arraycopy(c_result, 4, positive_activations, 0, positive_activations.length);
                res.setOutput_decision("" + c_result[CLASSE]);
            }
        } else {
            // preproces
            to_classify = classifiers.get(classes.get(0)).preprocessImage(input);
            //System.arraycopy(input, 0, to_classify, 0, input.length);

            ArrayList<Future<double[]>> tester_list = new ArrayList<>();
            
            for (String c : classes) {
                // submit to threads !
                tester_list.add(threadPool.submit(new LetterWorker(classifiers.get(c), to_classify.clone())));
            }
            
            for (int i = 0; i < classes.size(); i++) {
                try // deal with the classification and populate Result object
                {
                    String key = classes.get(i);
                    c_result = tester_list.get(i).get();

                    // pass info
                    positive_activations[i] = c_result[POSITIVE];
                    negative_activations[i] = c_result[NEGATIVE];
                    confidences[i] = c_result[CONFIDENCE];
                    diffs_activations[i] = positive_activations[i] - negative_activations[i];
                    // decision is based on the diffs_activation (max value.. is the detected class!)
                    if (confidences[i] > detection_value) {
                        detection_value = confidences[i];
                        res.setOutput_decision(key);
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    LOGGER.log(Level.SEVERE, "Error calculating i: {0} calculations interruped {1}", new Object[]{i, ex});
                    return null;
                }
            }
        }
        res.setPositive_activations(positive_activations);
        res.setNegative_activations(negative_activations);
        res.setConfidences(confidences);
        res.setDiffs_activations(diffs_activations);

        //
        return res;
    }

    /**
     * Reads to_test as a path and processes every image with the loaded model.
     * It outputs relevant information about the classification
     *
     * @param to_test path to be tested
     * @throws IOException
     */
    public void batch(String to_test) {
        Path path = Paths.get(to_test);
        if (Files.isDirectory(path)) {
            
            try {
                for (Path img : Files.newDirectoryStream(path, new DirectoryStream.Filter<Path>() {
                    
                    @Override
                    public boolean accept(Path entry) throws IOException {
                        // filters non files
                        return Files.isRegularFile(entry);
                    }
                })) {
                    try {
                        Image tmp = new Image(img.getFileName().toString(), "undef", ImageIO.read(img.toUri().toURL()), Snow.UNDEFINED);
                        
                        Result res = test(tmp.getPixels());
                        
                        LOGGER.log(Level.INFO, "{0} Ouput result {1}", new Object[]{img.getFileName().toString(), res.getOutput_decision()});
                        // write results
                        Path res_p = Paths.get(to_test, "results");
                        if (!Files.exists(res_p)) {
                            res_p = Files.createDirectory(res_p);
                        }
                        
                        FileWriter fw = new FileWriter("results//" + img.getFileName().toString() + "_test.csv");
                        fw.append(res.toString());
                        fw.close();
                        //
                        for (int i = 0; i < classes.size(); i++) {
                            LOGGER.log(Level.INFO, "{0} Confidence: {1} Positive: {2}  Negative {3}  Diffs {4} label {5}", new Object[]{classes.get(i),
                                res.getConfidences()[i], res.getPositive_activations()[i], res.getNegative_activations()[i], res.getDiffs_activations()[i], res.getOutput_decision()});
                        }
                    } catch (IOException ex) {
                        LOGGER.log(Level.SEVERE, null, ex);
                    }
                    
                    LOGGER.log(Level.INFO, "---------------");
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
    public OCRer getOcrer() {
        return ocrer;
    }
    
    public void setOcrer(OCRer ocrer) {
        this.ocrer = ocrer;
    }
    
    public HashMap<String, Classifier> getClassifiers() {
        return classifiers;
    }
    
    public void setClassifiers(HashMap<String, Classifier> classifiers) {
        this.classifiers = classifiers;
    }
    
    public ArrayList<String> getClasses() {
        return classes;
    }
    
    public void setClasses(ArrayList<String> classes) {
        this.classes = classes;
    }

//    public LetterWizard getCopy() {
//        LetterWizard res = new LetterWizard();
//        
//        for(String s: classes){
//            res.classes.add(""+s);
//            
//        }
//        
//        for(Classifier c : classifiers.values()){
//            
//        }
//        
//        return res;
//    }
}
