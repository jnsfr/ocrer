/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.model;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author jnunomac
 */
public class Image {

    
    private String filename;
    //
    private String label;
    // class
    private int classe;
    // pixels
    private int[] pixels;

    public Image(String filename,String label,BufferedImage img, int classe) {
        this.filename = filename;
        this.label = label;
        // conver to grayscale
        BufferedImage gray = new BufferedImage(img.getWidth(), img.getHeight(),
                BufferedImage.TYPE_BYTE_GRAY);

        Graphics2D g = gray.createGraphics();
        g.drawImage(img, 0, 0, null);
        //

        this.pixels = gray.getData().getPixels(0, 0, img.getWidth(), img.getHeight(), (int[]) null);
        this.classe = classe;
        // JNCOR: debug
//        int i = 0, j = 0;
//        for (int pixel : pixels) {
//
//            System.out.println("(" + i + "," + j + ") = " + pixel + " "+((pixel*5/256)));
//
//            i++;
//            if (i == img.getWidth()) {
//                i = 0;
//                j++;
//            }
//        }
        //System.exit(0);
        // JNCOR END
    }

//    public Image(int[] pixels, int classe) {
//        this.pixels = pixels;
//        this.classe = classe;
//
//    }

    public int getClasse() {
        return classe;
    }

    public void setClasse(int classe) {
        this.classe = classe;
    }

    public int[] getPixels() {
        return pixels;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    

}
