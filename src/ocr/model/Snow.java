package ocr.model;
/*
 * @author Andres Alvarez @edited João Correia Very simple version of SNOW 
 */

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import static ocr.OCRMain.LOGGER;
import ocr.config.SnowConfig;
import ocr.learn.Classifier;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import static ocr.learn.Classifier.NEGATIVE;
import static ocr.learn.Classifier.POSITIVE;

public class Snow implements Classifier {

    public static final boolean DEBUG = true;

    double[] scores;    // Final integers that hold the class labels.
    //public static final int GROUNDTRUTH_INDEX = 0;
//    private boolean modelPoint_iscalculated;

    public enum modelPoint {

        MAXPOSITIVE, MAXNEGATIVE, MINNEGATIVE, MINPOSITIVE, MAXABSDIFF, MINABSDIFF, MAXDIFFNEG, MINDIFFNEG, MAXDIFFPOS, MINDIFFPOS
    };
    //

    public double centroid_negativeactivation;
    public double centroid_positiveactivation;
    public double[][] modelpoints;
    public double[] positiveactivations;
    public double[] negativeactivations;

//    // jncor to refactor
//    // holds the "ideal" weight image
//    public double[] I;
//    public double Ipos;
//    public double Ineg;
//    public boolean I_iscalculated;
    //
    public int height;
    public int width;
    public int vals;
    // Parameters for the Snow algorithm.
//    public double alpha;
//    public double beta;
//    public double thresh;
//    public double init;
//    public int r; // number of cycles
    // The weights used for classifying.
    public double[][] weights;
    // for the original snow implementation in c++ 
    int[][] updates;
    int[][] active;
    public int num_pos;// # pos trained 
    public int num_neg;// # neg trained
    private String label;
//    private final boolean threshold_relative;
    public SnowConfig config_obj;
    // flag that signals if any change to the network was made during the cycle.. it stops if no change is made
    boolean any_update;

    /*
     * Constructer for the Snow classifier. Makes a 2 Winnow SNOW Network with
     * the given dimensions. Also loads it with the default parameters
     */
    public Snow(int width, int height, int vals) {
        this.config_obj = new SnowConfig();

        this.width = width;
        this.height = height;
        this.vals = vals;
        weights = new double[2][height * width * vals];
        active = new int[2][height * width * vals];
        updates = new int[2][height * width * vals];
        scores = new double[4];
//        I = new double[height * width];
//        boolean I_iscalculated = false;
        // Filling the weight tables with the initial weight.
        // The weights used for classifying.

        // Default params.
        //
        // 1.5, 0.7, 400.0, 2.0 : 0-1 paper!
        // 1.1, 0.82, 1600.0 2.0 // from the author
        // default_init_weight = 3 * threshold / average example size
//        setParams(1.1, 0.82, 1600.0);
//        setInitVals(2.0);
        //setParams(1.5, 0.7, 400.0);
        // basicamente activação 0.5
        // 
        //
        //default
        setParams(1.5f, 0.7f, (width * height * 5 * 0.25f));

        //setParams(1.25, 0.5, 400);
        //setInitVals((3 * config_obj.threshold) / (width * height));
        setInitVals(.1f);
        //setInitVals(2.992518703242);
    }

//    /**
//     * Constructs SNOW from the given file.
//     *
//     * @param width
//     * @param height
//     * @param vals
//     * @param label
//     * @param snowFile A file with a SNOW network inside.
//     * @exception IOException Thrown if the file can't be loaded
//     */
//    public Snow(int width, int height, int vals, String label) throws IOException {
//        // Getting the dimensions for the network.
//        this.width = width;
//        this.height = height;
//        this.vals = vals;
//        this.label = label;
//
//        //Making the network
//        weights = new double[3][height * width * vals];
//        active = new int[3][height * width * vals];
//        updates = new int[3][height * width * vals];
//
//        scores = new double[4];
//
//    }

    /**
     * Method to easily set the major parameters of the SNOW Network
     *
     * @param alpha A double to set the alpha (promotion)
     * @param beta A double to set the beta (demotion)
     * @param thresh A double to set the thresh (threshold)
     */
    public void setParams(double alpha, double beta, double thresh) {
        this.config_obj.alpha = alpha;
        this.config_obj.beta = beta;
        this.config_obj.threshold = thresh;
    }

    /**
     * Resets the snow arrays to their original values.
     */
    public void resetSnow() {

        num_neg = 0;
        num_pos = 0;
        active = new int[3][height * width * vals];
        updates = new int[3][height * width * vals];
        scores = new double[4];
        setParams(config_obj.alpha, config_obj.beta, config_obj.threshold);
        setInitVals(config_obj.initial_value);
    }

    /**
     * Sets the initial value to the input and resets all snow weights at
     * java.util.Random.nextInt(Random.java:250) to it.
     *
     * @param initVal The value to set all weights in the network to.
     */
    public void setInitVals(double initVal) {
        config_obj.initial_value = initVal;
        Arrays.fill(weights[POSITIVE], config_obj.initial_value);
        Arrays.fill(weights[NEGATIVE], config_obj.initial_value);
//        this.makeAggArray();
    }

    /**
     * Runs the SNOW Algorithm on an single image of the same type(label) trains
     * the network if instructed to.
     *
     *
     * @param sample
     * @param label
     */
    public void trainSingleInstance(int[] sample, int label) {

        // Set the curr_score as 0, go through and find the appropriate weights and
        // sums them for each class.
        scores[0] = scores[POSITIVE] = scores[NEGATIVE] = scores[CONFIDENCE] = 0;

        for (int i = 0; i < sample.length; i++) {
            int index = i * vals + (int) sample[i];
            //System.out.println("i: " + i + " value: " + sample[i] + " -> index: " + index);
            scores[POSITIVE] += weights[POSITIVE][index];
            scores[NEGATIVE] += weights[NEGATIVE][index];
            active[POSITIVE][index]++;
            active[NEGATIVE][index]++;

        }
        //System.exit(0);

        if (label == POSITIVE) {
            num_pos++;
//            LOGGER.log(Level.FINER, "pos {0}", curr_score[POSITIVE]);
            if (scores[POSITIVE] < config_obj.threshold) {
                // FN
                changeWeights(scores[POSITIVE], weights[POSITIVE], sample, config_obj.alpha, updates[POSITIVE]);
                any_update = true;
            }
//            LOGGER.log(Level.FINER, "neg: {0}", curr_score[NEGATIVE]);
            if (scores[NEGATIVE] >= config_obj.threshold) // False positive for Non
            {
                // FP
                changeWeights(scores[NEGATIVE], weights[NEGATIVE], sample, config_obj.beta, updates[NEGATIVE]);
                any_update = true;
            }
        } else if (label == NEGATIVE) {
            // DEBUG
//            System.out.println("VIM AQUI");
//            System.exit(0);
            num_neg++;
            if (scores[NEGATIVE] < config_obj.threshold) // False Negative for Non
            {
                changeWeights(scores[NEGATIVE], weights[NEGATIVE], sample, config_obj.alpha, updates[NEGATIVE]);
                any_update = true;
            }
            if (scores[POSITIVE] >= config_obj.threshold) // False Positive for Pos
            {
                changeWeights(scores[POSITIVE], weights[POSITIVE], sample, config_obj.beta, updates[POSITIVE]);
                any_update = true;
            }
        }

    }

    /**
     * Method to run Snow in train or test mode. In train mode it is given a
     * label and will correct the weights if the classification is wrong. In
     * testing the label is not used, and just the initial score is returned.
     *
     * @param sample An int[] representing the sample to train/test the
     * classifier on.
     * @return A double that is the ratio of the POSITIVE score divided by the
     * NEGATIVE score.
     */
    @Override
    public double[] classifySingleInstance(int[] sample) {

        // forced preprocess
        //sample = preprocessImage(sample);
//        FDFeatureExtractormain.LOGGER.log("fired");
        double[] curr_scores = new double[4];
        curr_scores[POSITIVE] = curr_scores[NEGATIVE] = curr_scores[CLASSE] = curr_scores[CONFIDENCE] = 0;

        //Arrays.stream(weights[POSITIVE]).parallel().reduce(new DoubleBinaryOperator)
        //
        //seq
        int index;
        curr_scores[POSITIVE] = curr_scores[NEGATIVE] = curr_scores[CLASSE] = curr_scores[CONFIDENCE] = 0;
        for (int i = 0; i < sample.length; i++) {
            index = i * vals + (int) sample[i];
            curr_scores[POSITIVE] += weights[POSITIVE][index];
            curr_scores[NEGATIVE] += weights[NEGATIVE][index];
        }
//        System.out.println("seq SCORES pos " + curr_score[POSITIVE]);
//        System.out.println("seq SCORES neg " + curr_score[NEGATIVE]);
        //
        if (curr_scores[POSITIVE] >= config_obj.threshold && curr_scores[POSITIVE] >= curr_scores[NEGATIVE]) {
            curr_scores[CLASSE] = POSITIVE;
        } else {
            if (curr_scores[NEGATIVE] >= config_obj.threshold && curr_scores[NEGATIVE] >= curr_scores[POSITIVE]) {
                curr_scores[CLASSE] = NEGATIVE;
            } else {
                curr_scores[CLASSE] = UNDEFINED;
            }
        }
        // for now.. 
        double diff_pos = centroid_positiveactivation - curr_scores[POSITIVE];
        double diff_neg = centroid_negativeactivation - curr_scores[NEGATIVE];
        curr_scores[CONFIDENCE] = (double) Math.sqrt((diff_pos * diff_pos) + (diff_neg * diff_neg));

        return curr_scores;
    }

    /**
     * Method used to alter the weights of either judge. Simply multiplies all
     * active weights by a set rate, assumably alpha or beta.
     *
     * @param activation to use if threshold-relative updating is in use
     * @param weights either classOne or classTwo
     * @param sample an int[] sample that holds the active weights.
     * @param rate a double, the value to multiply the active weights by.
     * @param updates
     */
    public void changeWeights(double activation, double[] weights, int[] sample, double rate, int[] updates) {
        for (int i = 0; i < sample.length; i++) {
            int index = i * vals + sample[i];
            if (config_obj.threshold_relative) {
                weights[index] = weights[index] * (rate * (config_obj.threshold / activation));
            } else {
                weights[index] *= rate;
            }

            updates[index]++;// keeps track of weights changes
        }
    }

//    /**
//     * Method to get the weights out of the SNOW classifer.
//     *
//     * @param label An int representing the class, should be POSITIVE(0) or
//     * NEGATIVE(1)
//     * @return either classOne or classTwo
//     */
//    public double[][] getWeights(int label) {
//        double[] retWeights = new double[width * height];
//        double[][] wei = new double[vals][width * height];
//        if (label == POSITIVE) {
//            retWeights = weights[POSITIVE];
//        } else if (label == NEGATIVE) {
//            retWeights = weights[NEGATIVE];
//        } else if (label == AGG) {
//            makeAggArray();
//            retWeights = weights[AGG];
//        }
//        int count = 0;
//        for (int i = 0; i < retWeights.length; i++) {
//            int sub = i % vals;
//            count = i / vals;
//            wei[sub][count] = retWeights[i];
//        }
//
//        return wei;
//    }
//    /**
//     * Constructs the array that has the difference between the positive network
//     * and the negative network weights.
//     */
//    public void makeAggArray() {
//        for (int i = 0; i < (height * width * vals); i++) {
//            weights[AGG][i] = weights[POSITIVE][i] - weights[NEGATIVE][i]; // additive comparison
//            //classAgg[i] = classOne[i]/classTwo[i]; // multiplicative comparison
//        }
//    }
    /**
     * Save the snow networks in a file. Each row has all of the positive
     * weights followed by all of the non-positive weights on that row.
     *
     * @param fileName The name of the file to save the networks to.
     */
    @Override
    public void save(Path fileName) throws IOException {

        FileWriter fstream = new FileWriter(fileName.toFile());
        BufferedWriter out = new BufferedWriter(fstream, 4096); // ~size
        out.write("\n");

        // target 0 1.000000000000 1.000000000000 5715 5715 winnow 0 1.500000000000 0.700000000000 400.000000000000 2.992518703242
        out.write("target " + NEGATIVE + " " + 1.0 + " " + 1.0 + " " + num_neg + " " + num_pos + " winnow 0 " + config_obj.alpha + " " + config_obj.beta + " " + config_obj.threshold + " " + config_obj.initial_value + "\n");
        int size = width * height * vals;
        for (int i = 0; i < size; i++) {

            // 0 :       0.0000000000 :          2 :        477         33       0.0010452936
            out.write("" + NEGATIVE + "  :   " + 0.0 + "  :   " + (2 + i)
                    + " :   " + active[NEGATIVE][i] + "      " + updates[NEGATIVE][i] + "     " + weights[NEGATIVE][i] + "\n");

        }

        out.write("\n\ntarget " + POSITIVE + " " + 1.0 + " " + 1.0 + " " + num_pos + " " + num_neg + " winnow 0 " + config_obj.alpha + " " + config_obj.beta + " " + config_obj.threshold + " " + config_obj.initial_value + "\n");

        for (int i = 0; i < size; i++) {

            // 0 :       0.0000000000 :          2 :        477         33       0.0010452936
            out.write("" + POSITIVE + "  :   " + 0.0 + "  :   " + (2 + i)
                    + " :   " + active[POSITIVE][i] + "      " + updates[POSITIVE][i] + "     " + weights[POSITIVE][i] + "\n");

        }
        out.write("\n\n");

        out.close();

    }

    /**
     * Loads this SNOW with weights and values in this file.
     *
     * @param fileName The name of the file that contains the snow network to
     * load
     * @throws IOException Because this method does IO
     */
    @Override
    public void load(Path fileName) throws IOException {
        config_obj = new SnowConfig();
        Scanner scan = new Scanner(fileName);
        String line;
        String[] splited;
        int t_vals = -1, i_val, i;
        while (scan.hasNextLine()) {
            line = scan.nextLine();
            if (line.matches("")) {
//                FDFeatureExtractormain.LOGGER.log("continue");
                continue;
            }
            if (line.startsWith("target")) {
//                System.out.println(line);
                splited = line.split(" ");
                t_vals = Integer.parseInt(splited[1].trim());
                if (t_vals == 0) {
                    num_neg = Integer.parseInt(splited[4].trim());
                    num_pos = Integer.parseInt(splited[5].trim());
                } else {
                    num_neg = Integer.parseInt(splited[5].trim());
                    num_pos = Integer.parseInt(splited[4].trim());
                }
                // 0      1  2   3  4 5  6     7  8   9    10     11
                // target 0 1.0 1.0 0 0 winnow 0 1.5 0.7 400.0 2.992518703242
                // target 0 1.0 1.0 14300 550 winnow 0 1.1 0.7 576.0 0.75
                config_obj.alpha = Float.parseFloat(splited[8].trim());
                config_obj.beta = Float.parseFloat(splited[9].trim());
                config_obj.threshold = Float.parseFloat(splited[10].trim());
                config_obj.initial_value = Float.parseFloat(splited[11].trim());
            } else {
                // 0 :       0.0000000000 :          4 :         54         20       0.0109643808
                if (line.startsWith("" + t_vals)) {
                    //FDFeatureExtractormain.LOGGER.log(line);
                    splited = line.split(":");

                    i_val = Integer.parseInt(splited[2].trim()) - 2;
                    //FDFeatureExtractormain.LOGGER.log(i_val);
                    splited = splited[3].trim().split(" ");
                    i = 0;
                    while (splited[i].matches("")) {
                        i++;
                    }
                    active[t_vals][i_val] = Integer.parseInt(splited[i].trim());
                    //FDFeatureExtractormain.LOGGER.log(active[t_vals][i_val]);
                    i++;
//                    for (i = 0; i < splited.length; i++) {
//                        FDFeatureExtractormain.LOGGER.log(splited[i] + " -> " + splited[i].matches(""));
//                    }
                    while (splited[i].matches("")) {
                        i++;
                    }
                    updates[t_vals][i_val] = Integer.parseInt(splited[i].trim());
                    //FDFeatureExtractormain.LOGGER.log(updates[t_vals][i_val]);
                    i++;
                    while (splited[i].matches("")) {
                        i++;
                    }
                    weights[t_vals][i_val] = Float.parseFloat(splited[i].trim());
                    //FDFeatureExtractormain.LOGGER.log(weights[t_vals][i_val]);
                }
            }
        }

//        int j;
//        for(i = 0 ; i < weights.length ;i++)
//        {
//            for(j = 0 ; j < weights[0].length;j++)
//            {
//                FDFeatureExtractormain.LOGGER.log(i+" : "+weights);
//            }
//        }
//        FDFeatureExtractormain.LOGGER.log("");
        scan.close();
//        makeAggArray();
        // calculate the I image for the loaded classifier
//        calculateI();
        calculateModelPoints();
//        calculateMinNeg();
//        calculateMaxPos();
    }

    /**
     *
     * @return enumeration element representing the type of classifier
     */
    @Override
    public Classifiertype getClassifierType() {
        return Classifiertype.SNOW;
    }

    /**
     * Preprocess input image pixels. In this case it quantizes the pixels in
     * this.vals .values
     *
     * @param pixels
     * @return a new array containing the altered pixels
     */
    @Override
    public int[] preprocessImage(int[] pixels) {
        int[] res = new int[pixels.length];
        for (int i = 0; i < pixels.length; i++) {
            res[i] = (pixels[i] * vals) / 256;
        }
        return res;
    }

    @Override
    public void trainInstances(ArrayList<int[]> sample, ArrayList<Integer> label) {
        int label_i;
        // for trace
        double fourth = config_obj.cycles * 0.25f;
//        LOGGER.log(Level.FINER, "{0} - config {1}", new Object[]{this.getLabel(), this.config_obj});
        for (int i = 0; i < config_obj.cycles; i++) {
            label_i = 0;
            any_update = false;
            if (i % (int) fourth == 0) {
                LOGGER.log(Level.FINER, "{0} - cycle {1}", new Object[]{this.getLabel(), i});
            }
            for (int[] current : sample) {
                trainSingleInstance(preprocessImage(current), label.get(label_i));
                label_i++;
            }
            if (!any_update) {
                LOGGER.log(Level.INFO, "No update was made to the network of {0} . Ending in cycle: {1}", new Object[]{this.getLabel(), i});
                break;
            }

        }
        // update model points
//        calculateI();
        calculateModelPoints();
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public ArrayList<double[]> classifyInstances(ArrayList<int[]> sample) {
        ArrayList<double[]> results = new ArrayList<>();
        double tmp[];

        for (int[] current : sample) {
            tmp = classifySingleInstance(preprocessImage(current));
//            System.out.println(tmp[POSITIVE]);
            // must be a deep copy
            double to_add[] = new double[4];
            System.arraycopy(tmp, 0, to_add, 0, tmp.length);
//            to_add[POSITIVE] = tmp[POSITIVE];
//            to_add[NEGATIVE] = tmp[NEGATIVE];
//            to_add[CLASSE] = tmp[CLASSE];
//            to_add[CONFIDENCE] = tmp[CONFIDENCE];
            //

            results.add(to_add);
        }
        return results;
    }

    @Override
    public void readConfig(Path to_load) throws IOException {
        PropertiesConfiguration config_file_ref;
        try {
            config_file_ref = new PropertiesConfiguration(to_load.toFile());

            this.config_obj = new SnowConfig();

            config_obj.alpha = config_file_ref.getDouble("alpha");
            config_obj.beta = config_file_ref.getDouble("beta");
            config_obj.threshold = config_file_ref.getDouble("threshold");
            config_obj.cycles = config_file_ref.getInt("cycles");
            config_obj.threshold_relative = config_file_ref.getBoolean("threshold_relative");
            config_obj.initial_value = config_file_ref.getDouble("initial_value");

            // reset snow!
            resetSnow();

            LOGGER.log(Level.INFO, "Loaded Config: {0}", config_obj);
        } catch (ConfigurationException ex) {
            LOGGER.log(Level.SEVERE, "error reading config: ", ex);
        }

    }

    public SnowConfig getConfig_obj() {
        return config_obj;
    }

    public void setConfig_obj(SnowConfig config_obj) {
        this.config_obj = config_obj;
    }

    private void calculateModelPoints() {
        LOGGER.log(Level.INFO, "Calculating the model points for {0}", this.label);
        int size_modelpoints = modelPoint.values().length;
        // make it as debug!
        modelpoints = new double[size_modelpoints][width * height];
        positiveactivations = new double[size_modelpoints];
        negativeactivations = new double[size_modelpoints];
        int best_i, I_index = 0, curr_point;
        double max, min, value = 0;

        // go from weights of each pixel that has "vals" possible weights
        for (int i = 0; i < weights[0].length; i += vals) {

            // for each modelpoint
            for (modelPoint point : modelPoint.values()) {
                // reset 
                best_i = 0;
                max = -Float.MAX_VALUE;
                min = Float.MAX_VALUE;
                // gets the enum indice
                curr_point = point.ordinal();

                for (int j = i; j < i + vals; j++) {
                    // maximize the diference of updated weigths
                    //if (updates[POSITIVE][j] > 0) {
                    //System.out.println("w_pos: " + weights[POSITIVE][j]);
                    //System.out.println("w_neg: " + weights[NEGATIVE][j]);

                    switch (point) {
                        case MAXABSDIFF:
                        case MINABSDIFF:
                            value = Math.abs(weights[POSITIVE][j] - weights[NEGATIVE][j]);
                            break;
                        case MAXNEGATIVE:
                        case MINNEGATIVE:
                            value = weights[NEGATIVE][j];
                            break;
                        case MAXPOSITIVE:
                        case MINPOSITIVE:
                            value = weights[POSITIVE][j];
                            break;
                        case MAXDIFFNEG:
                        case MINDIFFNEG:
                            value = weights[NEGATIVE][j] - weights[POSITIVE][j];
                            break;
                        case MAXDIFFPOS:
                        case MINDIFFPOS:
                            value = weights[POSITIVE][j] - weights[NEGATIVE][j];
                            break;

                    }
                    // ou mudar para contains MAX
                    if (point.compareTo(modelPoint.MAXABSDIFF) == 0 || point.compareTo(modelPoint.MAXNEGATIVE) == 0 || point.compareTo(modelPoint.MAXPOSITIVE) == 0
                            || point.compareTo(modelPoint.MAXDIFFNEG) == 0 || point.compareTo(modelPoint.MAXDIFFPOS) == 0) {
                        //System.out.println("diff: " +diff);
                        if (value > max) {
                            max = value;
                            best_i = j;
                            //System.out.println("updated: best_i: " + best_i + " diffs:" + diff);
                        }
                    } else {
                        // ou mudar para contains MIN
                        if (point.compareTo(modelPoint.MINABSDIFF) == 0 || point.compareTo(modelPoint.MINNEGATIVE) == 0 || point.compareTo(modelPoint.MINPOSITIVE) == 0
                                || point.compareTo(modelPoint.MINDIFFNEG) == 0 || point.compareTo(modelPoint.MINDIFFPOS) == 0) {
                            //System.out.println("diff: " +diff);
                            if (value <= min) {
                                min = value;
                                best_i = j;
                                //System.out.println("updated: best_i: " + best_i + " diffs:" + diff);
                            }
                        }
                    }
                    //}
                }
                // get the gray level according to vals (number of colors)
                modelpoints[curr_point][I_index] = (best_i % vals) * (255 / (vals - 1));
                //System.out.println("max i: " + best_i + " i_index:" + I_index + " value: " + I[I_index]);

                negativeactivations[curr_point] += weights[NEGATIVE][best_i];
                positiveactivations[curr_point] += weights[POSITIVE][best_i];
            }
            //System.out.println("neg: " + Ineg);
            //System.out.println("pos:" + Ipos);
            //System.exit(0);
            I_index++;
        }

//        this.modelPoint_iscalculated = true;
        for (modelPoint point : modelPoint.values()) {
            String name = point.name();
            int modelpoint_i = point.ordinal();
            LOGGER.log(Level.INFO, "[{3}]  modelpoint[{4}] - {0} ----  positive activation {1} negative activation {2}", new Object[]{name, positiveactivations[modelpoint_i], negativeactivations[modelpoint_i], label, modelpoint_i});
            // debug write image
            if (DEBUG) {
                BufferedImage diffsimg = new BufferedImage(this.width, this.height, BufferedImage.TYPE_BYTE_GRAY);
                Raster diffs_inputRaster = diffsimg.getData();
                WritableRaster diffs_outputRaster = diffs_inputRaster.createCompatibleWritableRaster();
                diffs_outputRaster.setPixels(0, 0, this.width, this.height, modelpoints[point.ordinal()]);
                diffsimg.setData(diffs_outputRaster);
                try {
                    ImageIO.write(diffsimg, "png", new File("" + getLabel() + "_" + point.name() + ".png"));
                } catch (IOException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }

        // calculate centroid 
        // in this case a triangle
        //double w_a =1f, w_b =1f, w_c = 1f;
        // 3 points choosen ..  min neg ,  max diff, min diff
        centroid_positiveactivation = (positiveactivations[modelPoint.MINNEGATIVE.ordinal()]
                + positiveactivations[modelPoint.MAXABSDIFF.ordinal()]
                + positiveactivations[modelPoint.MINABSDIFF.ordinal()])
                / 3;

        centroid_negativeactivation = (negativeactivations[modelPoint.MINNEGATIVE.ordinal()]
                + negativeactivations[modelPoint.MAXABSDIFF.ordinal()]
                + negativeactivations[modelPoint.MINABSDIFF.ordinal()])
                / 3;
        LOGGER.log(Level.INFO, "[{2}]  ----  centroid positive activation {0} centroid negative activation {1}", new Object[]{centroid_positiveactivation, centroid_negativeactivation, label});

        // polygon http://en.wikipedia.org/wiki/Centroid#cite_note-5
        // todo
        //
    }

}
