/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.model;


/**
 *
 * @author jnunomac
 */
public class OCRer {

    private int w;
    private int h;
    private int n_colors;
    private String strat;
    private ClassifierInfo[] path_to_models;
    private int n_classes;

    public OCRer() {

    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getN_colors() {
        return n_colors;
    }

    public void setN_colors(int n_colors) {
        this.n_colors = n_colors;
    }

    public ClassifierInfo[] getPath_to_models() {
        return path_to_models;
    }

    public void setPath_to_models(ClassifierInfo[] path_to_models) {
        this.path_to_models = path_to_models;
    }

    public String getStrat() {
        return strat;
    }

    public void setStrat(String strat) {
        this.strat = strat;
    }

    public int getN_classes() {
        return n_classes;
    }

    public void setN_classes(int n_classes) {
        this.n_classes = n_classes;
    }
    
    

}
