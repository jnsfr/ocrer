/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.model;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import static ocr.OCRMain.LOGGER;
import ocr.config.SVMConfig;
import ocr.config.SnowConfig;
import ocr.external.libsvm.Svm;
import ocr.external.libsvm.Svm_model;
import ocr.external.libsvm.Svm_node;
import ocr.external.libsvm.Svm_parameter;
import ocr.external.libsvm.Svm_print_interface;
import ocr.external.libsvm.Svm_problem;
import ocr.learn.Classifier;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * @author jnunomac
 */
public class SVM implements Classifier {

    private final int width;
    private final int height;
    private final int distinct_values;

    //////////////////
    private Svm_model svm;
    private String label;
    private SVMConfig config_obj;

    /**
     *
     * @param width
     * @param height
     * @param distinct_values
     */
    public SVM(int width, int height, int distinct_values) {
        this.width = width;
        this.height = height;
        this.distinct_values = distinct_values;
        this.config_obj = new SVMConfig();
    }

    @Override
    public double[] classifySingleInstance(int[] sample) {
        double[] curr_scores = new double[4 + svm.nr_class]; // neg actication class conf
        double[] prob_estimates = new double[svm.nr_class];
        double classification;

        // copy to the problem
        Svm_problem problem = new Svm_problem();
        problem.l = 1;
        problem.x = new Svm_node[problem.l][];
//        problem.y = new double[problem.l];
//        problem.y[0] = 0;
        problem.x[0] = new Svm_node[sample.length];
        for (int k = 0; k < sample.length; k++) {
            problem.x[0][k] = new Svm_node();
            problem.x[0][k].value = sample[k];
            problem.x[0][k].index = k + 1;
        }

        classification = Svm.svm_predict_probability(svm, problem.x[0], prob_estimates);

        int i = 4;
        curr_scores[CLASSE] = classification;
        for (double prob : prob_estimates) {
            // confidence
            if (classification == i) {
                curr_scores[CONFIDENCE] += prob;
            } else {
                curr_scores[CONFIDENCE] -= prob;
            }
            curr_scores[i] = prob;
            i++;

        }

        return curr_scores;
    }

    @Override
    public Classifiertype getClassifierType() {
        return Classifiertype.SVM;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public void save(Path to_save) {
        try {
            Svm.svm_save_model(to_save.toString(), svm);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "SVM - error saving model", ex);
        }
    }

    @Override
    public void load(Path to_load) {
        try {
            svm = Svm.svm_load_model(to_load.toString());
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "SVM error loading SVM ", ex);
        }
    }

    @Override
    public void trainInstances(ArrayList<int[]> sample, ArrayList<Integer> labels) {

        int n_examples = sample.size();
        LOGGER.log(Level.INFO, "[Svm] Training with : {0} ", n_examples);

        // build problem
        Svm_problem prob = new Svm_problem();
        prob.l = n_examples;
        prob.x = new Svm_node[prob.l][];
        prob.y = new double[prob.l];
        ArrayList<Integer> different_classes = new ArrayList<>();
        int[] tmp;
        for (int i = 0; i < n_examples; i++) {

            tmp = preprocessImage(sample.get(i));
//            prob.y[i] = labels.get(i) == POSITIVE ? 1 : -1;

            if (!contains(different_classes, labels.get(i))) {
                different_classes.add(labels.get(i));
            }

            prob.y[i] = labels.get(i);
            // 1 -positive class  -1 negative class
            prob.x[i] = new Svm_node[tmp.length];
//                System.out.print(prob.y[p_size + i] + " ");
            for (int j = 0; j < tmp.length; j++) {
                prob.x[i][j] = new Svm_node();
                prob.x[i][j].value = tmp[j];
                prob.x[i][j].index = j + 1;
//                    System.out.print(prob.x[p_size + i][j].index + ":" + prob.x[p_size + i][j].value + " ");
            }
//                System.out.println();

        }
        // train

        if (this.config_obj.optimize) {
            LOGGER.log(Level.INFO, "------------ Optimize Params -------------");
            copyParams(optimizeSvmParameters(prob, config_obj, 10));
            LOGGER.log(Level.INFO, "------------ END Optimize Params -------------");
        }
        String error_msg = Svm.svm_check_parameter(prob, config_obj);
        if (error_msg != null) {
            System.err.print("ERROR: " + error_msg + "\n");
            System.exit(1);
        }
        LOGGER.log(Level.INFO, "------------ Training SVM ... ------------");
        this.svm = Svm.svm_train(prob, config_obj);
        LOGGER.log(Level.INFO, "------------ END Training SVM ... ------------");

//        // save model 
//        Date model_f= new Date();
//        if(){
//            save(Paths.get(new SimpleDateFormat("yyyyMMddHHmmss").format(model_f) + "_svm_" + this.label + ".svm"));
//        }
        // test against training..
        double[] prob_estimates = new double[different_classes.size()];
        double v;
        int correct = 0;
//        System.out.println("---------------------");

        for (int i = 0; i < n_examples; i++) {
            StringBuilder b = new StringBuilder();
            //d = Svm.svm_predict(svm, prob.x[0]);
            v = Svm.svm_predict_probability(svm, prob.x[i], prob_estimates);

            b.append("example[").append(i).append("]  label: ").append(prob.y[i]).append(" predicted: ").append(v);

            different_classes.stream().forEach((cl) -> {
                b.append(" prob[").append(cl).append("]: ").append(prob_estimates[cl]);
            });
            if (prob.y[i] == v) {
                correct++;
            }
            LOGGER.log(Level.INFO, "{0}", b.toString());

        }

//        System.out.println("---------------------");
        LOGGER.log(Level.INFO, "Total correct: {0}/{1}", new Object[]{correct, n_examples});

    }

    @Override
    public ArrayList<double[]> classifyInstances(ArrayList<int[]> sample) {
        ArrayList<double[]> results = new ArrayList<>();
        double tmp[];

        for (int[] current : sample) {
            tmp = classifySingleInstance(preprocessImage(current));
//            System.out.println(tmp[POSITIVE]);
            // must be a deep copy
            double to_add[] = new double[4];
            System.arraycopy(tmp, 0, to_add, 0, tmp.length);
//            to_add[POSITIVE] = tmp[POSITIVE];
//            to_add[NEGATIVE] = tmp[NEGATIVE];
//            to_add[CLASSE] = tmp[CLASSE];
//            to_add[CONFIDENCE] = tmp[CONFIDENCE];
            //

            results.add(to_add);
        }
        return results;
    }

    @Override
    public void readConfig(Path to_load) throws IOException {
        PropertiesConfiguration config_file_ref;
        try {
            config_file_ref = new PropertiesConfiguration(to_load.toFile());

            this.config_obj = new SVMConfig();

            // build default
//            initSvmParams();
            //
            config_obj.svm_type = config_file_ref.getInt("svm_type", Svm_parameter.C_SVC);
            //config_obj.kernel_type = Svm_parameter.RBF;
            //config_obj.kernel_type = Svm_parameter.RBF;
            config_obj.kernel_type = config_file_ref.getInt("kernel_type", Svm_parameter.POLY);
            config_obj.degree = config_file_ref.getInt("degree", 2);
            config_obj.gamma = config_file_ref.getDouble("gamma", 1.0);
            config_obj.coef0 = config_file_ref.getDouble("coef0", 0.0);
            config_obj.nu = config_file_ref.getDouble("nu", 0.5);
            config_obj.cache_size = config_file_ref.getDouble("cache_size", 512);
            config_obj.C = config_file_ref.getDouble("C", 4);
            config_obj.eps = config_file_ref.getDouble("eps", 1e-3);
            config_obj.p = config_file_ref.getDouble("p", 0.1);
            config_obj.shrinking = config_file_ref.getInt("shrinking", 1);
            config_obj.probability = config_file_ref.getInt("probability", 1);
            config_obj.nr_weight = 0;// not an option right now
            config_obj.weight_label = new int[0];
            config_obj.weight = new double[0];

            // read the others
            config_obj.optimize = config_file_ref.getBoolean("optimize", false);
            config_obj.C0 = config_file_ref.getInt("C0", -10);
            config_obj.C1 = config_file_ref.getInt("C1", 10);
            config_obj.gamma0 = config_file_ref.getInt("gamma0", -5);
            config_obj.gamma1 = config_file_ref.getInt("gamma1", 5);
            config_obj.degree0 = config_file_ref.getInt("degree0", 2);
            config_obj.degree1 = config_file_ref.getInt("degree1", 5);

            LOGGER.log(Level.INFO, "Loaded Config: {0}", config_obj);
        } catch (ConfigurationException ex) {
            LOGGER.log(Level.SEVERE, "error reading config: ", ex);
        }
    }

    /**
     * Preprocess input image pixels. In this case it quantizes the pixels in
     * this.vals .values
     *
     * @param pixels
     * @return a new array containing the altered pixels
     */
    @Override
    public int[] preprocessImage(int[] pixels) {
        int[] res = new int[pixels.length];
        for (int i = 0; i < pixels.length; i++) {
            res[i] = (pixels[i] * distinct_values) / 256;
        }
        return res;
    }

    ///////////////////// Not from the interface
    /**
     * Attempts to optimize params with nr_fold cross-validation folds
     *
     * @param prob
     * @param param
     * @param nr_fold
     * @return
     */
    private Svm_parameter optimizeSvmParameters(Svm_problem prob, Svm_parameter param, int nr_fold) {
        double g, c;
        Svm_parameter best = new Svm_parameter();
        int best_res = 0, tmp;
        int o_step = 0;
        // optimize gamma and C in the form of log2c and log2g
        //  logc -> -1:3 (matlab sample)
        // logg -> -4:1
        //      	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
        //		2 -- radial basis function: exp(-gamma*|u-v|^2)\n"

        for (int d = this.config_obj.degree0; d < this.config_obj.degree1 + 1; d++) {
            param.degree = d;
            for (c = this.config_obj.C0; c < this.config_obj.C1 + 1; c++) {
                param.C = Math.pow(2, c); // optei por cobrir os valores do gráfico
                for (g = this.config_obj.gamma0; g < this.config_obj.gamma1 + 1; g++) {
                    LOGGER.log(Level.FINE, "------------------ Optimize step {0} ------------------", o_step);
                    param.gamma = Math.pow(2, g);
                    tmp = do_cross_validation(prob, param, nr_fold);
                    if (tmp > best_res) {
                        best.C = param.C;//Math.pow(2, c);
                        best.gamma = param.gamma;//Math.pow(2, g);
                        best.degree = d;
                        best_res = tmp;

                    }
                    LOGGER.log(Level.FINE, "------------------ BEST so far C: {0} gamma: {1} degree: {2} res: {3}------------------", new Object[]{best.C, best.gamma, best.degree, 100.0 * best_res / prob.l});
                    o_step++;
                    System.gc();
                }
            }
        }
        System.gc();
        // copy rest of the params..
        best.svm_type = param.svm_type;
        best.kernel_type = param.kernel_type;
        best.coef0 = param.coef0;
        best.nu = param.nu;
        best.cache_size = param.cache_size;
        best.eps = param.eps;
        best.p = param.p;
        best.shrinking = param.shrinking;
        best.probability = param.probability;
        best.nr_weight = param.nr_weight;
        best.weight_label = param.weight_label;
        best.weight = param.weight;
        //
        LOGGER.log(Level.INFO, "Optimization ENDED ! -> BEST C: {0} Gamma: {1} with {2} % correct", new Object[]{best.C, best.gamma, (100.0 * best_res / prob.l)});
        return best;
    }

    private int do_cross_validation(Svm_problem prob, Svm_parameter param, int nr_fold) {
        int i;
        int total_correct = 0;

        double[] target = new double[prob.l];

        Svm.svm_cross_validation(prob, param, nr_fold, target);

        for (i = 0; i < prob.l; i++) {
            if (target[i] == prob.y[i]) {
                ++total_correct;
            }
        }
        LOGGER.log(Level.FINE, "-------------------------------------------------------------------");
        LOGGER.log(Level.FINE, "Cross Validation Accuracy = {0}%", 100.0 * total_correct / prob.l);
        LOGGER.log(Level.FINE, "-------------------------------------------------------------------");

        return total_correct;
    }

    private void copyParams(Svm_parameter to_copy) {

        this.config_obj.svm_type = to_copy.svm_type;
        this.config_obj.kernel_type = to_copy.kernel_type;
        this.config_obj.coef0 = to_copy.coef0;
        this.config_obj.nu = to_copy.nu;
        this.config_obj.cache_size = to_copy.cache_size;
        this.config_obj.eps = to_copy.eps;
        this.config_obj.p = to_copy.p;
        this.config_obj.shrinking = to_copy.shrinking;
        this.config_obj.probability = to_copy.probability;
        this.config_obj.nr_weight = to_copy.nr_weight;
        this.config_obj.weight_label = to_copy.weight_label;
        this.config_obj.weight = to_copy.weight;
        this.config_obj.C = to_copy.C;
        this.config_obj.gamma = to_copy.gamma;
        this.config_obj.degree = to_copy.degree;
    }

    /**
     * Lambda contains method!
     *
     * @param different_classes
     * @param to_search
     * @return
     */
    private boolean contains(ArrayList<Integer> different_classes, Integer to_search) {
        return different_classes.parallelStream().anyMatch((one) -> {
            return one.compareTo(to_search) == 0;
        });
    }
}
