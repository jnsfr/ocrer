/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.model;

/**
 *
 * @author jnunomac
 */
public class Result {

    private String output_decision;
    private double[] positive_activations;
    private double[] negative_activations;
    private double[] confidences;
    private double[] diffs_activations;

    public Result() {

    }

    public String getOutput_decision() {
        return output_decision;
    }

    public void setOutput_decision(String output_decision) {
        this.output_decision = output_decision;
    }

    public double[] getPositive_activations() {
        return positive_activations;
    }

    public void setPositive_activations(double[] positive_activations) {
        this.positive_activations = positive_activations;
    }

    public double[] getNegative_activations() {
        return negative_activations;
    }

    public void setNegative_activations(double[] negative_activations) {
        this.negative_activations = negative_activations;
    }

    public double[] getConfidences() {
        return confidences;
    }

    public void setConfidences(double[] confidences) {
        this.confidences = confidences;
    }

    public double[] getDiffs_activations() {
        return diffs_activations;
    }

    public void setDiffs_activations(double[] diffs_activations) {
        this.diffs_activations = diffs_activations;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < this.diffs_activations.length; i++) {
            b.append(this.positive_activations[i]).append(",").append(this.negative_activations[i]).append("\n");
        }
        return b.toString();
    }

}
