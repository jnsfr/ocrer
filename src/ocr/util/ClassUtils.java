/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import static ocr.OCRMain.LOGGER;

/**
 *
 * @author jnunomac
 */
public class ClassUtils {

    public static String getClassToString(Object the_obj, Class<?> a_class) {
        StringBuilder res = new StringBuilder();
        Iterable<Field> fields = getFieldsUpTo(a_class, null);
        res.append(a_class).append(" {\n");
        for (Field e : fields) {
            e.setAccessible(true);
            try {
                res.append(e.getName()).append(" = ").append(e.get(the_obj)).append("\n");
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        res.append("}");
        return res.toString();
    }

    public static Iterable<Field> getFieldsUpTo(Class<?> startClass, Class<?> exclusiveParent) {
        List<Field> currentClassFields;
        currentClassFields = new ArrayList(Arrays.asList(startClass.getDeclaredFields()));
        Class<?> parentClass = startClass.getSuperclass();

        if (parentClass != null && (exclusiveParent == null || !(parentClass.equals(exclusiveParent)))) {
            List<Field> parentClassFields = (List<Field>) getFieldsUpTo(parentClass, exclusiveParent);
            currentClassFields.addAll(parentClassFields);
        }

        return currentClassFields;
    }
}
