/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.learn;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import static ocr.OCRMain.LOGGER;
import ocr.config.TrainerConfig;
import static ocr.config.TrainerConfig.ClassificationStrategy.OneVsAll;
import static ocr.config.TrainerConfig.ClassificationStrategy.OneVsOne;
import ocr.external.libsvm.Svm;
import static ocr.learn.Classifier.CONFIDENCE;
import static ocr.learn.Classifier.NEGATIVE;
import static ocr.learn.Classifier.POSITIVE;
import ocr.learn.Classifier.Classifiertype;
import ocr.model.ClassifierInfo;
import ocr.model.Image;
import ocr.model.OCRer;
import ocr.model.SVM;
import ocr.model.Snow;
import ocr.params.OCRerTrainerParams;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * @author jnunomac
 */
public class TrainOCRer {

    // concurrent stuff
    public final int NUM_THREADS;
    private final ExecutorService threadPool;
    //

    // "brains"
    private final ArrayList<String> classes;
    private final HashMap<String, Classifier> classifiers;
    private final ArrayList<Path> dirs;
    private final ConcurrentHashMap<String, ArrayList<Image>> instances_list;

    // for output
    OCRer ocr_model;
    Classifiertype type = Classifiertype.SNOW;
    boolean output_features = true;
    //
//    private int num_classes;
    private final OCRerTrainerParams params;
    public String label;
//    private Date time;
    private TrainerConfig cfg;

    // cache mechanisms
    // public static int MAX_LOADED_FILES = 1000;
    //
    public TrainOCRer() {
        this.NUM_THREADS = 8;
        this.threadPool = Executors.newFixedThreadPool(NUM_THREADS);
        this.classes = new ArrayList<>();
        this.classifiers = new HashMap<>();
        this.dirs = new ArrayList<>();
        this.instances_list = new ConcurrentHashMap<>();
        this.params = new OCRerTrainerParams();
    }

    public TrainOCRer(OCRerTrainerParams train) {
        // static for now
        this.params = train;
        this.NUM_THREADS = train.numthreads;
        this.threadPool = Executors.newFixedThreadPool(NUM_THREADS);
        this.classes = new ArrayList<>();
        this.classifiers = new HashMap<>();
        this.dirs = new ArrayList<>();
        this.instances_list = new ConcurrentHashMap<>();
    }

    public void batchTrain() throws IOException, InterruptedException, ExecutionException {
//        this.time = new Date();
        this.cfg = readConfig(Paths.get(params.path_to_ocr_config));
        Path path = Paths.get(params.path_to_data);
        this.label = path.getFileName().toString();
        // contains the paths for later assess

//        this.num_classes = path.toFile().listFiles(new FileFilter() {
//            @Override
//            public boolean accept(File pathname) {
//                return pathname.isDirectory();
//            }
//        }).length;
        //// load instances
        loadInstances(path);
        //// train each classifier with the instances
        trainClassifiers();

        // debug.. prolly disable at some point (or make it an option)
        drawWeightImages();

        // save the OCR!
        saveModel(Paths.get(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "trained_" + label));

        // shutdown
        // dont forget to shutdown threads ! (..else it will hang... FOREVER!)
        threadPool.shutdown();
        // wait for all of them to complete the last task..
        threadPool.awaitTermination(1, TimeUnit.DAYS);
        LOGGER.log(Level.INFO, "~~~ENDED~~~");
    }

    private void trainClassifiers() throws InterruptedException, ExecutionException, IOException {
        String the_path = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "models_" + label;
        if (!Files.exists(Paths.get(the_path))) {
            Files.createDirectory(Paths.get(the_path));
        }

        HashMap<String, Trainer> trainer_ref = new HashMap<>();
        ArrayList<Future<Classifier>> trainers_lists = new ArrayList<>();
        if (this.cfg.classif_strat == OneVsOne) {
            LOGGER.log(Level.INFO, "OneVsOne strat!");
            /////// ONE vs ONE !
            Classifier new_model = createClassifier();
            new_model.setLabel("multiclass_" + classes.size());

            Trainer tr = new Trainer(new_model, this.cfg.classif_strat, instances_list, output_features, the_path);

            try {
                tr.call();
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
                System.exit(-1);
            }
            LOGGER.log(Level.INFO, "{0} was trained", new_model.getLabel());
            // put the trained classifier in the hashmap
            classifiers.put(new_model.getLabel(), new_model);

        } else {
            LOGGER.log(Level.INFO, "OneVsAll strat!");
            /////// ONE VS ALL
            // for each class
            for (String classe : classes) {
                // Negatives do not need a classifier 
                if (!classe.matches("Negatives")) {

                    // create classifier (choose which one)
                    Classifier new_model = createClassifier();
                    // sets the label
                    new_model.setLabel(classe);
                    // creates reference to trainer thread.. that we want to keep to access some info later
                    Trainer tr = new Trainer(new_model, this.cfg.classif_strat, instances_list, output_features, the_path);
                    trainer_ref.put(new_model.getLabel(), tr);
                    // go trainer thread !
                    trainers_lists.add(threadPool.submit(tr));
                }
            }

            // retrieve trained classifiers
            boolean all_ok = true;
            for (Future<Classifier> a_loader : trainers_lists) {
                Classifier tmp = a_loader.get();
                if (tmp != null) {
                    LOGGER.log(Level.INFO, "{0} was trained", tmp.getLabel());
                    // put the trained classifier in the hashmap
                    classifiers.put(tmp.getLabel(), tmp);
                } else {
                    LOGGER.log(Level.SEVERE, "A Classifier is null for some reason.. checking which at the end");
                    all_ok = false;
                }
            }

            if (!all_ok) {
                // check which went ok.. and which not
                classes.stream().forEach((classe) -> {
                    LOGGER.log(Level.WARNING, "{0} - {1}", new Object[]{classe, classifiers.containsKey(classe)});
                });
            }

            // summary
            writeSummary(trainer_ref);

            // perform OCR eval on training!
            ocrEval(trainer_ref);
        }

        System.gc();

    }

    private void ocrEval(HashMap<String, Trainer> trainer_ref) throws IOException {

        LOGGER.log(Level.INFO, "Training Evaluation ...");
        FileWriter pfw = new FileWriter(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "performance_" + label + ".csv");
        pfw.append(",,");
        for (int i = 0; i < classes.size(); i++) {
            if (classes.get(i).matches("Negatives")) {
                classes.remove(i);
                i--;
                continue;
            }
            // ,, because its a quad col !
            pfw.append(classes.get(i)).append(",,,,");
        }
        pfw.append("\n");
        pfw.append("example,groundtruth,");
        for (String c : classes) {
            pfw.append("positive,negative,difference,confidence,");
        }
        pfw.append("max_activation,max_activation_diffs,correct_by_activation,correct_by_activationdiffs\n");
        // for each example ! (going for the same order that classes.get(0) went)
        Collection<ClassificationInfo> tmp = trainer_ref.get(classes.get(0)).getInfo().values();

        // Order not guaranteed .. but we can sort it later !
        for (ClassificationInfo example : tmp) {
            if (!example.ground_truth.matches("Negatives")) {

                // setup the starting info
                double max_activation = -Float.MIN_VALUE;
                double max_activation_diffs = -Float.MAX_VALUE;
                String classe_activ = "undefined", classe_activdiffs = "undefined";
                pfw.append("" + example.fname);
                pfw.append(",");
                pfw.append("" + example.ground_truth);
                pfw.append(",");
                // for each classifier
                for (int i = 0; i < trainer_ref.size(); i++) {
                    // the same example (filename is the key!)
                    String classif_label = classes.get(i);
                    if (classif_label.matches("Negatives")) {
                        continue;
                    }
                    ClassificationInfo info = trainer_ref.get(classif_label).getInfo().get(example.fname);
                    if (info == null) {
                        LOGGER.log(Level.SEVERE, "Training example key is null.. something is wrong!! IGNORING");
                        continue;
                    }
                    pfw.append("" + info.values[POSITIVE]);
                    pfw.append(",");
                    pfw.append("" + info.values[NEGATIVE]);
                    pfw.append(",");

                    if (info.values[POSITIVE] > max_activation) {
                        max_activation = info.values[POSITIVE];
                        classe_activ = "" + classif_label;
                    }
                    double diff = info.values[POSITIVE] - info.values[NEGATIVE];
                    pfw.append("" + diff);
                    pfw.append(",");
                    pfw.append("" + info.values[CONFIDENCE]);
                    pfw.append(",");
                    if (diff > max_activation_diffs) {
                        max_activation_diffs = diff;
                        classe_activdiffs = "" + classif_label;
                    }
                }
                pfw.append("" + classe_activ);
                pfw.append(",");
                pfw.append("" + classe_activdiffs);
                pfw.append(",");
                pfw.append("" + classe_activ.matches(example.ground_truth));
                pfw.append(",");
                pfw.append("" + classe_activdiffs.matches(example.ground_truth));
                pfw.append("\n");
//            pfw.flush();
//            System.gc();
            }
        }
        LOGGER.log(Level.INFO, "End Training Evaluation ...");
        pfw.close();
    }

    private void writeSummary(HashMap<String, Trainer> trainer_ref) throws IOException {
        // write summary .. and extract performance as an OCR unit
        LOGGER.log(Level.INFO, "Writing summary...");
        FileWriter fw = new FileWriter(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "summary_" + label + ".csv");
        fw.append("model,positives,negatives,undefined,percentage_positives,percentage_negatives,percentage_undefined,correct_examples,incorrect_examples\n");
        for (Trainer tr_ref : trainer_ref.values()) {
            fw.append(tr_ref.getStats_summary()).append("\n");
        }
        fw.close();
        LOGGER.log(Level.INFO, "Wrote summary...");
        System.gc();
    }

    public void loadInstances(Path path) throws IOException, InterruptedException, ExecutionException {
        // prepare the list for the threads
        ArrayList<Future<ArrayList<Image>>> loaders_lists = new ArrayList<>();
        //ArrayList<Future<ArrayList<int[]>>> loaders_lists = new ArrayList<>();
        // final list of list of instances 

        // load instances
        for (Path in : Files.newDirectoryStream(path, Files::isDirectory // filters non directories
        )) {
            dirs.add(in);
            // start a thread per folder !
            loaders_lists.add(threadPool.submit(new ImageLoader(in)));
        }

        // retrieve results
        for (Future<ArrayList<Image>> a_loader : loaders_lists) {
            ArrayList<Image> tmp = a_loader.get();
            if (!tmp.isEmpty()) {
                LOGGER.log(Level.INFO, "Adding: {0} instances: {1}", new Object[]{tmp.get(0).getLabel(), tmp.size()});
                // key will be the dir (classe).. that represents the class for the instances
                instances_list.put(tmp.get(0).getLabel(), tmp);
                classes.add(tmp.get(0).getLabel());
            }
        }
        LOGGER.log(Level.INFO, "Before gc");
        System.gc();
    }

    /**
     * Draws a pixel representation of the weights of a SNOW classifier.
     *
     * @param weights
     * @param m to draw its weights
     * @throws IOException if something went wrong writing the images
     */
    protected void drawWeightImage(Path weights, Classifier m) throws IOException {
        if (m.getClassifierType() != Classifier.Classifiertype.SNOW) {
            LOGGER.log(Level.WARNING, "Calling a method only supported with a SNoW classifier.. ending..");
            return;
        }

        Snow model = (Snow) m;

        // positive 1 negative 0
        double min;
        double max;
        int size = cfg.width * cfg.height;
        double[] pixel_weight = new double[size];
        int[] diff_pixels = new int[size];
        int p;
        boolean first = true;
        int[] pixels = new int[size];

        for (int i = 0; i < 2; i++) {
            min = Float.MAX_VALUE;
            max = Float.MIN_VALUE;
            p = 0;
            // compute min and max..
            //System.out.println("w_size " + ocr_model.weights[i].length);
            for (int j = 0; j < model.weights[i].length; j++) {
                //System.out.println(a.weights[i][j]);
                pixel_weight[p] += model.weights[i][j];
                if ((j + 1) % cfg.distinct_values == 0) {

                    if (pixel_weight[p] < min) {
                        min = pixel_weight[p];
                    }
                    if (pixel_weight[p] > max) {
                        max = pixel_weight[p];
                    }
//                    System.out.println("pixel_weight[" + p + "] = " + pixel_weight[p]);
                    p++;

                }
            }
            LOGGER.log(Level.FINER, "min :{0}", min);
            LOGGER.log(Level.FINER, "max :{0}", max);

            BufferedImage img = new BufferedImage(cfg.width, cfg.height, BufferedImage.TYPE_BYTE_GRAY);
            Raster inputRaster = img.getData();

            WritableRaster outputRaster = inputRaster.createCompatibleWritableRaster();

            pixels = inputRaster.getPixels(0, 0, cfg.width, cfg.height, (int[]) null);// jncor mudei!

            // build weight map..
            for (int x = 0; x < pixel_weight.length; x++) {
                //                OldRange = (OldMax - OldMin)
                //NewRange = (NewMax - NewMin)
                //NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin

                pixels[x] = (int) (((pixel_weight[x] - min) * 255) / (max - min)) + 0; // converte intervalo
                if (first) {
                    diff_pixels[x] = pixels[x];
                }
            }
            first = false;

            outputRaster.setPixels(0, 0, cfg.width, cfg.height, pixels);
            img.setData(outputRaster);
            ImageIO.write(img, "png", new File(weights.toFile(), "snowweights_" + model.getLabel() + "_" + (i == POSITIVE ? "positive" : "negative") + ".png"));
        }

        BufferedImage diffsimg = new BufferedImage(cfg.width, cfg.height, BufferedImage.TYPE_BYTE_GRAY);
        Raster diffs_inputRaster = diffsimg.getData();

        BufferedImage absdiffsimg = new BufferedImage(cfg.width, cfg.height, BufferedImage.TYPE_BYTE_GRAY);
        Raster absdiffs_inputRaster = diffsimg.getData();

        WritableRaster diffs_outputRaster = diffs_inputRaster.createCompatibleWritableRaster();
        WritableRaster absdiffs_outputRaster = diffs_inputRaster.createCompatibleWritableRaster();
        int[] diffs_pixels = diffs_inputRaster.getPixels(0, 0, cfg.width, cfg.height, (int[]) null);
        int[] absdiffs_pixels = absdiffs_inputRaster.getPixels(0, 0, cfg.width, cfg.height, (int[]) null);
        //model.makeAggArray();

        // get the min and max
        min = Float.MAX_VALUE;
        max = Float.MIN_VALUE;
        for (int x = 0; x < pixel_weight.length; x++) {
            //pixels[x] = diff_pixels[x] - pixels[x]; // NEGATIVE - POSITIVE
            diffs_pixels[x] = pixels[x] - diff_pixels[x]; // POSITIVE - NEGATIVE
            if (diffs_pixels[x] < min) {
                min = diffs_pixels[x];
            }
            if (diffs_pixels[x] > max) {
                max = diffs_pixels[x];
            }
        }
        LOGGER.log(Level.FINER, "---DIFFS---");
        LOGGER.log(Level.FINER, "min :{0}", min);
        LOGGER.log(Level.FINER, "max :{0}", max);

        // conver to pixel intensities
        for (int x = 0; x < pixel_weight.length; x++) {

            //diffs_pixels[x] = (int) (((diffs_pixels[x] - min) * 255) / (max - min)) + 0; // converte intervalo
            if (diffs_pixels[x] > 0) { // absolute diference
                absdiffs_pixels[x] = 255;
            } else {
                absdiffs_pixels[x] = 0;
            }
            diffs_pixels[x] = (int) (((diffs_pixels[x] - min) * 255) / (max - min)) + 0; // converte intervalo

        }

        diffs_outputRaster.setPixels(0, 0, cfg.width, cfg.height, diffs_pixels);
        diffsimg.setData(diffs_outputRaster);
        ImageIO.write(diffsimg, "png", new File(weights.toFile(), "snowweights_" + model.getLabel() + "_diffs.png"));

        absdiffs_outputRaster.setPixels(0, 0, cfg.width, cfg.height, absdiffs_pixels);
        absdiffsimg.setData(absdiffs_outputRaster);
        ImageIO.write(absdiffsimg, "png", new File(weights.toFile(), "snowweights_" + model.getLabel() + "_absolute_diffs.png"));

    }

    private void drawWeightImages() throws IOException {
        // prepare folder
        final Path weights = Paths.get(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "weights_" + label);
        if (!Files.exists(weights)) {
            Files.createDirectories(weights);
        }
        for (final Classifier classif : classifiers.values()) {
            threadPool.submit(new Runnable() {
                // inline deployment of a thread
                @Override
                public void run() {
                    try {
                        drawWeightImage(weights, classif);
                    } catch (IOException ex) {
                        LOGGER.log(Level.SEVERE, "IO problems when creating the weight image of {1} . error: {0}", new Object[]{ex, classif.getLabel()});
                    }
                }
            });

        }
    }

    private Classifier createClassifier() throws IOException {
        Classifier to_return;
        if (cfg.classifier_type == Classifiertype.SNOW) {
            to_return = new Snow(cfg.width, cfg.height, cfg.distinct_values);
        } else {
            to_return = new SVM(cfg.width, cfg.height, cfg.distinct_values);
            Svm.svm_set_print_string_function((String to_log) -> {
                // sets the interface to log the trainning !
                LOGGER.log(Level.FINEST, "{0}", to_log);
            });

        }

        // read config!
        to_return.readConfig(Paths.get(params.path_to_config_file));
        return to_return;
        // old
        //return params.type == Classifiertype.SNOW.ordinal() ? new Snow(w, h, n_colors, cycles) : new SVMOneVsALL();
    }

    /**
     * Generates the OCR model to a specified output_path.
     *
     * @param output_path the folder path for the OCRer and its classifiers
     * @throws IOException
     */
    public void saveModel(Path output_path) throws IOException {

        // prepare folder
        if (!Files.exists(output_path)) {
            Files.createDirectories(output_path);
        }
        // save the main configuration with pointers to the others and the type

        // for now all have this filename (param?)
        Path main_file = Paths.get(output_path.toString(), "ocrer.model");

        //// output info
        OCRer to_save = new OCRer();
        to_save.setH(cfg.height);
        to_save.setW(cfg.width);
        to_save.setN_colors(cfg.distinct_values);
        to_save.setStrat(cfg.classif_strat.toString());
        ClassifierInfo cinfo[] = new ClassifierInfo[classifiers.size()];
        // save each ocr_model
        if (this.cfg.classif_strat == OneVsAll) {
            int p = 0;
            for (String key : classes) {
                LOGGER.log(Level.INFO, "Writting {0} to {1}", new Object[]{key, Paths.get(output_path.toString(), key)});
                Classifier c = classifiers.get(key);
                cinfo[p] = new ClassifierInfo();
                cinfo[p].setLabel(key);
                // for now its equal to the key... but it can be edited to be elsewhere
                cinfo[p].setPath(key);
                cinfo[p].setType(c.getClassifierType().toString());
                p++;
                c.save(Paths.get(output_path.toString(), key));
            }
        } else {
            int p = 0;
            Classifier c = classifiers.get("multiclass_" + classes.size());
            cinfo[p] = new ClassifierInfo();
            cinfo[p].setLabel(c.getLabel());
            // for now its equal to the key... but it can be edited to be elsewhere
            cinfo[p].setPath(c.getLabel());
            cinfo[p].setType(c.getClassifierType().toString());
            c.save(Paths.get(output_path.toString(), c.getLabel()));
        }
        to_save.setPath_to_models(cinfo);
        /////

        // json mode "readable"
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // convert to json !
        String output = gson.toJson(to_save);
        // fast save !
        Files.write(main_file, output.getBytes());
    }

    public TrainerConfig readConfig(Path p) {
        TrainerConfig to_load_cfg;
        PropertiesConfiguration config_file_ref;
        try {
            config_file_ref = new PropertiesConfiguration(p.toFile());

            to_load_cfg = new TrainerConfig();

            to_load_cfg.height = config_file_ref.getInt("height");
            to_load_cfg.width = config_file_ref.getInt("width");
            to_load_cfg.distinct_values = config_file_ref.getInt("distinct_values");
            to_load_cfg.classifier_type = Classifiertype.valueOf(config_file_ref.getString("classifier_type"));
            to_load_cfg.classif_strat = TrainerConfig.ClassificationStrategy.valueOf(config_file_ref.getString("classification_strategy"));

            LOGGER.log(Level.INFO, "Loaded Config: {0}", to_load_cfg);
        } catch (ConfigurationException ex) {
            LOGGER.log(Level.SEVERE, "error reading config: ", ex);
            to_load_cfg = null;
        }
        return to_load_cfg;
    }

}
