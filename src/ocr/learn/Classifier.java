/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.learn;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 *
 * @author jnunomac
 */
public interface Classifier {

    public static final int NEGATIVE = 0;
    public static final int POSITIVE = 1;
    public static final int UNDEFINED = -1;
    public static final int CLASSE = 2;
    public static final int CONFIDENCE = 3;

    public enum Classifiertype {

        SNOW, SVM
    };

    /**
     * Method that will train the model with a group of instance.
     *
     * @param sample array with values for training
     * @param labels corresponding labels
     */
    public void trainInstances(ArrayList<int[]> sample, ArrayList<Integer> labels);

    /**
     * Classify sample and outputs its classification.
     *
     * @param sample
     * @return outputs from the classifier
     */
    public double[] classifySingleInstance(int[] sample);

    /**
     * Classify Instances and outputs data from the classifications
     *
     * @param sample
     * @return outputs from the classifier
     */
    public ArrayList<double[]> classifyInstances(ArrayList<int[]> sample);

    /**
     * Gets the type of the classifier. It is used to identify the classifier
     * and check specific situations where classifier behavior differs
     *
     * @return the type of classifier
     */
    public Classifiertype getClassifierType();

    /**
     * Getter for the label
     *
     * @return label that can be used to identify a classifier
     */
    public String getLabel();

    /**
     * Setter for the label
     *
     * @param label that can be used to identify a classifier
     */
    public void setLabel(String label);

    /**
     * Saves the model on target path. Saves the state of the object that
     * implements this interface
     *
     * @param to_save
     * @throws java.io.IOException
     */
    public void save(Path to_save) throws IOException;

    /**
     * Loads the model from target path. Restore the state of the object that
     * implements this interface
     *
     * @param to_load
     * @throws java.io.IOException
     */
    public void load(Path to_load) throws IOException;

    /**
     *
     * @param to_load
     * @throws IOException
     */
    public void readConfig(Path to_load) throws IOException;

    /**
     * Pre process the pixels of the input image in some way.
     *
     * @param pixels
     * @return pre_processed pixels - new array with the pre-processed pixels
     */
    public int[] preprocessImage(int[] pixels);

}
