/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.learn;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import static ocr.OCRMain.LOGGER;
import ocr.model.Image;
import ocr.model.Snow;

/**
 *
 * @author jnunomac
 */
public class ImageLoader implements Callable<ArrayList<Image>> {

    private final Path to_process;
    private final String name;

    public ImageLoader(Path to_process) {
        this.to_process = to_process;
        this.name = to_process.getFileName().toString();
    }

    @Override
    public ArrayList<Image> call() throws Exception {
        ArrayList<Image> instances = new ArrayList<>();
        LOGGER.log(Level.INFO, "Loading -> {0}", name);
        for (Path img : Files.newDirectoryStream(to_process, new DirectoryStream.Filter<Path>() {

            @Override
            public boolean accept(Path entry) throws IOException {
                // filters non files 
                return Files.isRegularFile(entry);
            }
        })) {
            try {
                // create a memory representation .. converted to grayscale .. with th class and original filename
                Image tmp = new Image(img.getFileName().toString(), name, ImageIO.read(img.toUri().toURL()), Snow.UNDEFINED);
                instances.add(tmp);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "ERROR -> {0}", e);
            }
        }

        return instances;
    }

    public Path getTo_process() {
        return to_process;
    }

    public String getName() {
        return name;
    }

}
