/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr.learn;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.logging.Level;
import static ocr.OCRMain.LOGGER;
import ocr.config.TrainerConfig;
import ocr.config.TrainerConfig.ClassificationStrategy;
import static ocr.learn.Classifier.CLASSE;
import static ocr.learn.Classifier.NEGATIVE;
import static ocr.learn.Classifier.POSITIVE;
import static ocr.learn.Classifier.UNDEFINED;
import ocr.model.Image;

/**
 *
 * @author jnunomac
 */
public class Trainer implements Callable<Classifier> {

    private final Classifier model;
    private final boolean output_features;
    private final ConcurrentHashMap<String, ArrayList<Image>> instances_list;
    private String stats_summary;
    private HashMap<String, ClassificationInfo> info;
    private final String output_path;
    private final ClassificationStrategy strat;

    public Trainer(Classifier model, ClassificationStrategy strat, ConcurrentHashMap<String, ArrayList<Image>> instances_list, boolean output_features, String the_path) {
        this.model = model;
        this.output_features = output_features;
        this.instances_list = instances_list;
        this.info = new HashMap<>();
        this.output_path = the_path;
        this.strat = strat;
    }

    @Override
    public Classifier call() throws Exception {

        //// trainOnevsAll the classifier
        if (strat == ClassificationStrategy.OneVsAll) {
            // group positive and negative
            // separate.. we might need that later..
            ArrayList<Image> positives = new ArrayList<>(instances_list.get(model.getLabel()));
            ArrayList<Image> negatives = new ArrayList<>();
            for (String in : instances_list.keySet()) {
                if (in.compareTo(model.getLabel()) != 0) {
                    negatives.addAll(instances_list.get(in));
                }
            }
            // trainOnevsAll  and test 
            trainOnevsAll(positives, negatives);
            testOnevsAll(positives, negatives);
        } else {
            trainOnevsOne();
        }

        // return updated model
        LOGGER.log(Level.INFO, "Classifier {0} trained!", model.getLabel());
        System.gc();
        return model;
    }

    private void savefeatures() {
        LOGGER.log(Level.INFO, "Saving features for {0}", model.getLabel());
    }

    private void trainOnevsAll(ArrayList<Image> positives, ArrayList<Image> negatives) {
        ArrayList<int[]> dataset = new ArrayList<>();
        ArrayList<Integer> labels = new ArrayList<>();
        int pos_size = positives.size(), neg_size = negatives.size();
        int i = 0;
        boolean more_pos = i < pos_size, more_neg = i < neg_size;
        // adds one of each class until it runs out of examples to add
        for (i = 0; more_pos || more_neg; i++) {

            if (more_pos) {

                // add to the dataset
                dataset.add(positives.get(i).getPixels());
                labels.add(POSITIVE);
            }
            if (more_neg) {
                // add to the dataset
                dataset.add(negatives.get(i).getPixels());
                labels.add(NEGATIVE);
            }
            // update conditions..
            more_pos = i + 1 < pos_size;
            more_neg = i + 1 < neg_size;
        }

        // (opt) save dataset to file
        if (output_features) {
            savefeatures();
        }
        LOGGER.log(Level.INFO, "{0} - Training  {1} examples", new Object[]{model.getLabel(), dataset.size()});
        model.trainInstances(dataset, labels);

    }

    //private void test(ArrayList<int[]> dataset) {
    private void testOnevsAll(ArrayList<Image> positives, ArrayList<Image> negatives) {
        FileWriter fw = null;
        try {

            ArrayList<int[]> p = new ArrayList<>();
            positives.stream().forEach((pos) -> {
                p.add(pos.getPixels());
            });

            ArrayList<int[]> n = new ArrayList<>();
            negatives.stream().forEach((neg) -> {
                n.add(neg.getPixels());
            });

            ArrayList<double[]> results_p = model.classifyInstances(p);
            ArrayList<double[]> results_n = model.classifyInstances(n);
            String fpath = Paths.get(output_path + "/_results_" + model.getLabel() + "_" + System.currentTimeMillis() + ".csv").toString();
            fw = new FileWriter(fpath);

            fw.append("example,positive_activation,negative_activation,class\n");
            // get stats from classification
            int c_pos = 0, c_neg = 0, c_undef = 0, correct = 0, incorrect = 0;
            double perc_pos, perc_neg, perc_undef;
            double[] tmp;
            // lazy... pos first
            int r_p_size = results_p.size();
            ClassificationInfo a_classif;
            for (int i = 0; i < r_p_size; i++) {
                tmp = results_p.get(i);
                // gather classification
                a_classif = new ClassificationInfo();
                a_classif.fname = positives.get(i).getFilename();
                a_classif.ground_truth = positives.get(i).getLabel();
                a_classif.values = new double[tmp.length];
                System.arraycopy(tmp, 0, a_classif.values, 0, tmp.length);
                //

                // save to csv more info!
                fw.append("" + positives.get(i).getFilename()).append(",");
                fw.append("" + a_classif.values[POSITIVE]).append(",");
                fw.append("" + a_classif.values[NEGATIVE]).append(",");

                if ((int) tmp[CLASSE] == UNDEFINED) {
                    fw.append("undefined");
                    c_undef++;
                    incorrect++;
                } else {
                    if ((int) tmp[CLASSE] == POSITIVE) {
                        fw.append("positive");
                        c_pos++;
                        // in this case.. because we are checking the positives in this cycle
                        correct++;
                    } else {
                        fw.append("negative");
                        c_neg++;
                        incorrect++;
                    }
                }
                fw.append("\n");
                // adds the stats
                info.put(a_classif.fname, a_classif);
            }
            // lazy negative last
            int r_n_size = results_n.size();
            for (int i = 0; i < r_n_size; i++) {
                tmp = results_n.get(i);

                // gather classification
                a_classif = new ClassificationInfo();
                a_classif.fname = negatives.get(i).getFilename();
                a_classif.ground_truth = negatives.get(i).getLabel();
                a_classif.values = new double[tmp.length];
                System.arraycopy(tmp, 0, a_classif.values, 0, tmp.length);
                //

                fw.append("" + negatives.get(i).getFilename()).append(",");
                fw.append("" + tmp[POSITIVE]).append(",");
                fw.append("" + tmp[NEGATIVE]).append(",");
                if ((int) tmp[CLASSE] == UNDEFINED) {
                    fw.append("undefined");
                    c_undef++;
                    incorrect++;
                } else {
                    if ((int) tmp[CLASSE] == POSITIVE) {
                        fw.append("positive");
                        c_pos++;
                        incorrect++;
                    } else {
                        fw.append("negative");
                        c_neg++;
                        // in this case.. because we are checking the negatives in this cycle
                        correct++;
                    }
                }
                fw.append("\n");
                // adds the stats
                info.put(a_classif.fname, a_classif);
            }

            // summary str
            perc_pos = ((double) c_pos / (double) r_p_size);
            perc_neg = ((double) c_neg / (double) r_n_size);
            perc_undef = ((double) c_undef / ((double) (r_p_size + r_n_size)));
            StringBuilder b = new StringBuilder();
            b.append(model.getLabel());
            b.append(",");
            b.append(c_pos);
            b.append(",");
            b.append(c_neg);
            b.append(",");
            b.append(c_undef);
            b.append(",");
            b.append(perc_pos);
            b.append(",");
            b.append(perc_neg);
            b.append(",");
            b.append(perc_undef);
            b.append(",");
            b.append(correct);
            b.append(",");
            b.append(incorrect);
            stats_summary = b.toString();
            // end summary str

        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getStats_summary() {
        return stats_summary;
    }

    public void setStats_summary(String stats_summary) {
        this.stats_summary = stats_summary;
    }

    public HashMap<String, ClassificationInfo> getInfo() {
        return info;
    }

    public void setInfo(HashMap<String, ClassificationInfo> info) {
        this.info = info;
    }

    private void trainOnevsOne() {
        ArrayList<int[]> dataset = new ArrayList<>();
        ArrayList<Integer> labels = new ArrayList<>();
        ArrayList<String> different_classes = new ArrayList<>();
        int size = this.instances_list.size();
        int i = 0;

        // adds one of each class until it runs out of examples to add
        this.instances_list.values().stream().forEach((img_set) -> {
            for (Image img : img_set) {
                // add example to dataset
                dataset.add(img.getPixels());
                int ind;
                ind = contains(different_classes, img.getLabel());
                if (ind != -1) {
                    labels.add(ind);
                } else {
                    different_classes.add(img.getLabel());
                    labels.add(different_classes.size() - 1);
                }
            }
        });

        // (opt) save dataset to file
        if (output_features) {
            savefeatures();
        }
        LOGGER.log(Level.INFO, "{0} - Training  {1} examples {2} classes", new Object[]{model.getLabel(), dataset.size(), different_classes.size()});
        model.trainInstances(dataset, labels);

    }

    /**
     * Lambda contains method!
     *
     * @param different_classes
     * @param to_search
     * @return
     */
    private boolean contains(ArrayList<Integer> different_classes, Integer to_search) {
        return different_classes.parallelStream().anyMatch((one) -> {
            return one.compareTo(to_search) == 0;
        });
    }

    /**
     * Lambda contains method!
     *
     * @param different_classes
     * @param to_search
     * @return
     */
    private int contains(ArrayList<String> different_classes, String to_search) {
        for (int i = 0; i < different_classes.size(); i++) {
            if (different_classes.get(i).compareTo(to_search) == 0) {
                return i;
            }
        }
        return -1;

    }
}
